import { CategoryList, CreateSubCategory, ErrorPage } from '../../../pages';

const routeListData = [
    { exact: true, path: '/', content: CategoryList, title: 'Categories'},
    { exact: true, path: '/create', content: CreateSubCategory, title: 'Create Sub-Categories'},
    { exact: false, path: '', content: ErrorPage, title: 'Error'}
];

export default routeListData;