import React from 'react';
import './index.css';
import TreeNode from './TreeNode';

const Tree = ({ data }) => {
    return (
        <div className="tree">
            <ul>
                {data.map(tree => (
                    <TreeNode key={tree.id} node={tree} />
                ))}
            </ul>
        </div>
    )
}

export default Tree;