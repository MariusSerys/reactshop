import React from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Tree from '.';

const TreeNode = ({node}) => {
    const history = useHistory();
    
    const hasChild = node.children ? node.children.length !== 0 ? true : false : false;
    const categoryId = node.id;
    
    return (
        <li>
            <div className="branch">
                <p>{node.name} { node.price && node.price !== '' ? `- ${node.price} EUR` : null} </p>
                { node.price && node.price !== '' ? null : (
                    <Button onClick={() => {
                        history.push({ pathname: '/create' ,
                        state: {
                            category: categoryId,
                            task: null,
                        }
                    });
                    }} variant="outline-info" size="sm">+</Button>
                ) }
            </div>
            { hasChild ? 
                <Tree data={node.children} /> :
                null
            }
        </li>
    )
}

export default TreeNode;
