import React from "react";
import { Card, Container } from "react-bootstrap";

export default function Header(props) {
  const { title } = props;
    return (
        <Container>
        <Card className="text-center">
          <Card.Header>
          <h1 className="display-1">React Shop</h1>
          </Card.Header>
          <Card.Body>
            <Card.Title>
              <h1 className="display-5">{title}</h1>
            </Card.Title>
            <Card.Text>
              Add sub-categories and add prices.
            </Card.Text>
          </Card.Body>
        </Card>
      </Container>
    );
}