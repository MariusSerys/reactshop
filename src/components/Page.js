import React from 'react';
import { Button, Container } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Header from './Header';

const Page = ({ content, title }) => {
    const history = useHistory();
    return (
        <Container
            fluid
            className='content d-flex flex-column min-vh-100'
        >
            <Header title={title} />
            <Container className='content d-flex'>
                <Button
                    className="ml-5"
                    variant="primary"
                    type="submit"
                    onClick={() => {
                        history.goBack();
                    }}
                >
                    Back
                </Button>
            </Container>
            
            {content()}
        </Container>
    );
};

export default Page;
