import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css";
import RouteList from "./components/RouteList";

function App() {
  return (
    <div className="App">
      <RouteList />
    </div>
  );
}

export default App;
