import React from "react";
import { Container, Row } from "react-bootstrap";
import Tree from "../../../components/Tree";
import { useShopContext } from "../../../context/ShopContext";


const Categories = () => {
    const { shopCategories } = useShopContext();
    return(
            <Container>
                <Row>
                    <Tree data={shopCategories} />
                </Row>
            </Container>
       
    )
}
export default Categories;