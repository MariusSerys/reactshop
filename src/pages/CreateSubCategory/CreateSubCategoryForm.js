import React, { useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useShopContext } from '../../context/ShopContext';

const CreateSubCategoryForm = () => {
    const { addNewCategory } = useShopContext();
    const [ pricingToggle, setPricingToggle ] = useState(true);
    const [ categoryData, setCategoryData ] = useState({
        name : '',
        price : '',
    });
    const history = useHistory();
    const { category } = history.location.state;

    const toggle = () => {
        pricingToggle ? setPricingToggle(false) : setPricingToggle(true);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        addNewCategory(category, categoryData);
        history.push({ pathname: '/' });
    }

    const handleChange = (event) => setCategoryData({
        ...categoryData,
        [event.target.name]: event.target.value,
    });
    return (
        <Container className='d-flex'>
            <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3" controlId="subCategory">
                <Row>
                    <Col>
                        <Form.Label column lg={2}>
                        Sub-category:
                        </Form.Label>
                    </Col>
                    
                    <Col>
                        <Form.Control
                            onChange={handleChange} 
                            value={categoryData.name}
                            name='name' type="text" 
                            required 
                        />
                    </Col>
                </Row>
                </Form.Group>
                <Form.Group className="mb-3" controlId="checkPricing">
                <Row>
                        <label lg={2}>
                        Check if you want to add pricing:
                        </label>
                        <input type="checkbox" label="Check if you want to add pricing" onChange={toggle} />
                </Row> 
                </Form.Group>
                <Form.Group className="mb-3" controlId="price">
                    <Row>
                        <Col>
                            <Form.Label column lg={2}>
                            Price:
                            </Form.Label>
                        </Col>
                        
                        <Col>
                            <Form.Control 
                            name='price' 
                            type="text" 
                            onChange={handleChange} 
                            value={categoryData.price}
                            disabled={pricingToggle} />
                        </Col>
                    </Row>
                </Form.Group>
                
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </Container>
        
    )
}
export default CreateSubCategoryForm;