import CategoryList from './CategoryList';
import CreateSubCategory from './CreateSubCategory';
import ErrorPage from './ErrorPage';

export {
    CategoryList,
    ErrorPage,
    CreateSubCategory
};

const Pages = {
    CategoryList,
    ErrorPage,
    CreateSubCategory
};

export default Pages;