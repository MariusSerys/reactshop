import React, { createContext, useCallback, useContext, useState } from "react";
import CategoryListData from './data/categorie-list-data.json';

const ShopContext = createContext();
const { Provider } = ShopContext;

const storage = window.localStorage;

const getValue = () => {
    try {
        // storage.removeItem('shopData');
        if (storage.getItem('shopData') === null) {
            storage.setItem('shopData', JSON.stringify(CategoryListData));
            return JSON.parse(storage.getItem('shopData'));
        }else {
            return JSON.parse(storage.getItem('shopData'));
        }
    } catch (error) {
       
        console.log(error);
        return [];
    }
}

const ShopContextProvider = ({ children }) => {
    
    const [ shopCategories, setShopCategories ] = useState(getValue());
    let arrayOfCategories = shopCategories;
    const addNewCategory = useCallback((categoryId, subCategory) => {
        
        arrayOfCategories.map(function findCategory(categoryObject) {
            if(categoryObject.id === categoryId){
                if(({}).toString.call(categoryObject.children) === '[object Array]') {
                     subCategory.id = `${categoryId}-${1 + categoryObject.children.length}`
                } else {
                    subCategory.id = `${categoryId}-1`;
                };
                categoryObject.children = ({}).toString.call(categoryObject.children) === '[object Array]' ? [...categoryObject.children, subCategory] : [subCategory];
            }
            ({}).toString.call(categoryObject.children) === '[object Array]' && categoryObject.children.map(findCategory)
        });
        
        
        storage.setItem('shopData', JSON.stringify(arrayOfCategories));
        setShopCategories(JSON.parse(storage.getItem('shopData')));
        
        
        
    }, []);

    return (
        <Provider value={{shopCategories, setShopCategories, addNewCategory}} displayName="ShopContextProvider">
            { children }
        </Provider>
    );

}
export default ShopContextProvider;

export const useShopContext = () => useContext(ShopContext);